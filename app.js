//
// WidgetBackupProcessor
//
// Backup widgets in AWS
//

// module includes
var moment = require('moment');
var AWS = require('aws-sdk');

var database = require('./database.js');

// configurables
var isDebug = false; // FLAG to switch between secor_dev and prod environments
var directoryList = [
    'partition/widget-click',
    'partition/widget-conversion',
    'partition/widget-impression-pixel',
    'partition/widget-impression',
    'partition_details/widget-impression'
]; // add more directories here if in contentad-rawlogs/secor_prod/partition

var logPath = '/var/log/widgetbackupprocessor-error.log'
var prefixPath = ((isDebug) ? 'secor_dev' : 'secor_prod');
var bucketName = 'contentad-rawlogs';
var targetBucketName = 'landingzone-all-files-to-load';
var queueCount = 0;
var intervalThread = null;

var s3 = null;

main();


function runProcess() {
    var bRequestMade = false;
    queueCount = 0;

    console.log('Running scheduled event...');

    s3 = new AWS.S3();

    for(var i = 0; i < directoryList.length; i++) {
        var dir = directoryList[i];

        var folderName = 'dt=' + moment().subtract(1, 'hours').format('YYYY-MM-DD-HH');
        var path = prefixPath + '/' + dir + '/' + folderName; // e.g. "secor_prod/partition/widget-click/dt=2015-03-26-16"



        (function(dir, folderName, path){
            // call another function to save the data for every async iteration

            s3.listObjects({
                Bucket: bucketName,
                Prefix: path,
                MaxKeys: 100000
            }, function (err, data) {
                if (err) {
                    // TBD

                    console.log('Error: ' + err.Message);
                } else {

                    if(data.Contents.length > 0) {

                        var destinationPath = prefixPath + '/' + dir + '/all_files_to_load/';
                        console.log('Deleting target directory first...');

                        for (var k = 0; k < data.Contents.length; k++) {
                            var item = data.Contents[k];

                            var sourcePath = bucketName + '/' + item.Key;
                            var destinationKey = dir + '/' + item.Key.split('/').pop();

                            queueCount++;
                            bRequestMade = true;
                            if(!database.recordExists(bucketName, item.Key)) {
                                database.insertRecord(bucketName, item.Key);

                                (function(k, item, sourcePath, destinationKey){
                                    // call another function to save the data for every async iteration
                                    console.log('copying ' + sourcePath + ' -> ' + targetBucketName + '/' + destinationKey);

                                    s3.copyObject({
                                        Bucket: targetBucketName,
                                        CopySource: encodeURIComponent(sourcePath),
                                        Key: destinationKey,
                                        ACL: 'bucket-owner-full-control'

                                    }, function (err, data) {
                                        if (err) {
                                            // TBD
                                            console.log('Error: ' + err.Message);

                                        } else {
                                            // success

                                        }

                                        queueCount--;
                                    });

                                })(k, item, sourcePath, destinationKey);
                            } else {
                                queueCount--;
                            }

                        }

                    } else {

                        // no results were found on server
                        console.log('No requests to be made!');

                        bRequestMade = true;
                        queueCount = 0;
                    }
                }

            });

        })(dir, folderName, path);
    }

    intervalThread = setInterval( function() {

        if(bRequestMade && queueCount < 1) {
            clearInterval(intervalThread);
            queueCount = 0;
            intervalThread = null;

            console.log('Finished!');
            database.saveDatabase();
        } else if(bRequestMade && queueCount > 0) {
            console.log('Items left to be processed: ' + queueCount);
        }


    }, 1000);

}


function main() {

    AWS.config.update({
        accessKeyId: "AKIAJNWEXNQM76Q6NN7A",
        secretAccessKey: "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",
        region: 'us-west-2'
    });

    console.log('Loading Flat Database...');
    database.loadDatabase();

    console.log('Starting...');
    runProcess(); // run the event now
}

function generateError(err) {
    var str = '[' + moment().format('YYYY-MM-DD HH:mm:ss') + '] ' + err + '\n';

    fs.appendFileSync(logPath, str);
}
