
// Required modules
var fs = require('fs-extra');
var moment = require('moment');

// globals
var databaseFile = '/home/procmanager/data.db';
var nexus = null; // primary database object during runtime

module.exports.loadDatabase = function() {

    if(!fs.existsSync(databaseFile)) {
        nexus = [];
    } else {
        nexus = [];

        try {
            nexus = JSON.parse(fs.readFileSync(databaseFile));
        } catch(ex) {
            nexus = [];
        }

    }

    if(nexus != null && nexus.length > 0) {
        if(parseInt(moment(nexus[0].actionTime).format('D')) < parseInt(moment().format('D'))) {
            // if today is a new day (e.g. yesterday was the 2nd and today is the 3rd) then flush records from previous day
            flushRecords();
        }
    }
};

function saveDatabase() {

    fs.writeFileSync(databaseFile, JSON.stringify(nexus));
}

module.exports.saveDatabase = function() {saveDatabase() };

module.exports.recordExists = function(bucketName, keyPath) {

    if(nexus != null && nexus.length > 0) {
        for(var k = 0; k < nexus.length; k++) {
            var item = nexus[k];

            if(item.bucket == bucketName &&
                item.keyPath == keyPath &&
                moment(item.actionTime).format('MM-DD-YYYY') == moment().format('MM-DD-YYYY')) {

                return true;
            }
        }
    }

    return false;
};

module.exports.insertRecord = function(bucketName, keyPath) {
    var item = {
        bucket: bucketName,
        keyPath: keyPath,
        actionTime: moment().format()
    };

    nexus.push(item);
};

function flushRecords() {
    console.log('Records Flushed!');

    nexus = [];
    saveDatabase();
}

